# css-pre-compile

#### 介绍
css预处理编译，本初心是可使用scss、less编写原生小程序样式

#### 软件架构
gulp sass less


#### 安装教程

1.  yarn add css-pre-compile -D or npm install css-pre-compile --save --dev


#### 使用说明

1.  在项目根目录创建compile.js文件，文件名可任意命名
2.  在compile.js中写入

```
const { start } = require('css-pre-compile')
start({
  globs: ['pages/**/*.scss'],
  extname: '.wxss'
})
```

3.  其中globs 为匹配的文件，extname 为编译所生成的后缀名
4.  然后打开命令窗口，使用node compile.js 命令去执行此文件即可

#### git
https://gitee.com/tobyliu/css-pre-compile
