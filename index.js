const { src, dest, watch, lastRun } = require('gulp')
const rename = require('gulp-rename')
const through = require('through2')
const initOptions = (options = {}) => {
  return {
    globs: options.globs || ['src/**/*.scss', 'src/**/*.less', 'pages/**/*.scss', 'pages/**/*.less'],
    extname: options.extname || '.wxss',
    ignoreImporters: options.ignoreImporters || ['.wxss']
  }
}
let options = initOptions()
// let ignoreImporters = ['.wxss', '.css']
function render (type= 'sass', input = '') {
  return new Promise((resolve, reject) => {
    if (type === 'less') {
      const less = require('less')
      less.render(input, (err, ouput) => {
        if (err) {
          reject(err)
        } else {
          resolve(ouput.css)
        }
      })
    } else if (type === 'sass') {
      const sass = require('sass')
      sass.render({
        data: input,
        importer: (url, prev, done) => {
          // console.log(prev)
          // console.log(done)
          if (options.ignoreImporters.some((item) => { return url.indexOf(item) > -1 })) {
            // console.log(url.replace(options.extname, '.css'))
            return done({
              contents: `@import "${url.replace(options.extname, '.css')}"`
            })
          }
          done({
            file: url
          })
        }
      }, (err, ouput) => {
        if (err) {
          reject(err)
        } else {
          // console.log(ouput.css.toString('utf-8').replace('.css', options.extname))
          // 替换
          let outputStr = ouput.css.toString('utf-8')
          const matches = outputStr.match(/@import[^;]+;/g)
          if (matches) {
            matches.forEach(item => {
              outputStr = outputStr.replace(item, item.replace('.css', options.extname))
            })
          }
          resolve(Buffer.from(outputStr))
        }
      })
    }
  })
    
}

function cssPreCompile () {
  const stream = through.obj(function (originalFile, unuse, cb) {
    let input = ''
    if (originalFile.isBuffer()) {
      input = originalFile.contents.toString()
    }
    const extname = originalFile.extname
    let type = 'sass'
    if (/less$/.test(extname)) {
      type = 'less'
    }
    console.log('\n===============================')
    console.log(`file # ${originalFile.path} # compiling`)
    render(type, input)
      .then((output) => {
        originalFile.contents = Buffer.from(output)
        console.log(`file # ${originalFile.path} # compile success`)
      })
      .catch(err => {
        console.log(err)
        console.log(`file # ${originalFile.path} # compile fail`)
       })
      .finally(() => {
        this.push(originalFile)
        cb()
      })
  })
  return stream
}






const task = () => {
  const { globs, extname } = options
  return src(globs, { buffer: true, since: lastRun(task) })
    .pipe(cssPreCompile())
    .pipe(rename({ extname: extname }))
    .pipe(dest(file => file.base))
}


const start = opt => {
  options = initOptions(opt)
  console.log(options)
  ignoreImporters = options.ignoreImporters
  console.log('start and watching....')
  watch(options.globs, { ignoreInitial: false }, task)
}
module.exports = {
  task: () => {
    start()
  },
  start: start
}